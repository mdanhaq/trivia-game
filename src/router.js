import VueRouter from "vue-router";
import Home from "@/components/Home/Home.vue";
import Question from "@/components/Question/Question.vue";
import Result from "@/components/Result/Result.vue";

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/question",
        name: "Question",
        component: Question,
    },
    {
        path: "/result",
        name: "Result",
        component: Result,
    },
];

const router = new VueRouter({ routes });

export default router;
